from abc import ABC
from enum import Enum


class AbstractIndicatorPlugin(ABC):

    class Status(Enum):
        ERROR = -1
        UNKNOWN = 0
        GREEN = 1
        YELLOW = 2
        RED = 3

    def __init__(self):
        self.last_state = self.Status.UNKNOWN

    def get_name(self):
        pass

    def get_status(self) -> Status:
        return self.last_state
