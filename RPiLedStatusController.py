import RPi.GPIO as GPIO
from SBahnStatusChecker import SBahnStatusChecker

sbahn_led = {'r_pin': 1, 'g_pin': 2, 'b_pin': 3}

GPIO.setmode(GPIO.BOARD)
GPIO.setup(sbahn_led.values(), GPIO.OUT, initial=GPIO.LOW)

sbahn_checker = SBahnStatusChecker()
sbahn_status = sbahn_checker.get_status()


def switch_to_red():
    GPIO.output(sbahn_led['r_pin'], GPIO.HIGH)
    GPIO.output(sbahn_led['g_pin'], GPIO.LOW)
    GPIO.output(sbahn_led['b_pin'], GPIO.LOW)


def switch_to_green():
    GPIO.output(sbahn_led['r_pin'], GPIO.LOW)
    GPIO.output(sbahn_led['g_pin'], GPIO.HIGH)
    GPIO.output(sbahn_led['b_pin'], GPIO.LOW)


def switch_to_yellow():
    GPIO.output(sbahn_led['r_pin'], GPIO.HIGH)
    GPIO.output(sbahn_led['g_pin'], GPIO.HIGH)
    GPIO.output(sbahn_led['b_pin'], GPIO.LOW)


if sbahn_status == sbahn_checker.Status.RED:
    switch_to_red()

if sbahn_status == sbahn_checker.Status.GREEN:
    switch_to_green()

if sbahn_status == sbahn_checker.Status.YELLOW:
    switch_to_yellow()
