It will check if:
+ There are S-Bahn disruption at given station in given direction
+ It will be raining on the way from home to station
+ It will be raining on the way from station to work
+ All above, but in opposit direction

And it will show the status on a LED connected to Raspberry Pi GPIO.