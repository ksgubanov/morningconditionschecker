import xmlschema

from AbstractIndicatorPlugin import AbstractIndicatorPlugin as aip
from TestData import TestData
from xml.etree import ElementTree
import datetime
import requests


class SBahnStatusChecker(aip):

    def __init__(self):
        super().__init__()
        self.eva = open('config/eva').read()  # Kornwestheim Pbf
        self.destination = open('config/destination').read()
        self.token_sandbox = open('config/token_sandbox').read()
        self.token_prod = open('config/token_prod').read()
        self.url = 'https://api.deutschebahn.com/timetables/v1/fchg/'+self.eva # get full changes
        self.token = self.token_sandbox
        self.dbahn_api_schema = xmlschema.XMLSchema('schema/Timetables_REST.xsd')
        self.sbahn_lines = ['4', '5']

    def get_status(self):
        headers = {'Accept': 'application/xml', 'Authorization': 'Bearer ' + self.token}
        x = requests.get(self.url, headers=headers)
        text = x.text
        #text = TestData.get_valid_test_xml()
        if x.status_code != 200:
        #if False:
            aip.last_state = self.Status.ERROR
        elif self.dbahn_api_schema.is_valid(text):
            if self.is_there_problems(text):
                aip.last_state = self.Status.RED
            else:
                aip.last_state = self.Status.GREEN
        return aip.last_state

    def get_name(self) -> str:
        return self.__class__.__name__

    @staticmethod
    def is_today(data_time):
        today = datetime.datetime.now().date()
        message_ts = datetime.datetime.strptime(data_time, '%y%m%d%H%M')
        return today == message_ts.date()

    def is_there_problems(self, xml_response):
        time_table = ElementTree.fromstring(xml_response)

        for s in time_table.iter('s'):
            for message in s.iter('m'):
                if message.attrib['t'] == 'h' and \
                        message.attrib['cat'].lower().find("störung") > -1 and \
                        self.is_today(message.attrib['ts']) and \
                        int(message.attrib['pr']) < 4:
                    for dp in s.iter('dp'):
                        if 'cpth' in dp.attrib and \
                                dp.attrib['cpth'].find(self.destination) > -1 and \
                                self.sbahn_lines.count(str(dp.attrib['l'])):
                            return True

        return False
